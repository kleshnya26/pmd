from PyQt5 import QtCore, QtGui, QtWidgets


class HelpWindow(QtWidgets.QWidget):
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.setLang()
        self.setupUi(self)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(744, 301)
        MainWindow.setStyleSheet("background-color: #E2F9FF;")
        self.heading_1 = QtWidgets.QLabel(MainWindow)
        self.heading_1.setGeometry(QtCore.QRect(40, 20, 661, 20))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.heading_1.setFont(font)
        self.heading_1.setAlignment(QtCore.Qt.AlignCenter)
        self.heading_1.setObjectName("heading_1")
        self.heading_2 = QtWidgets.QLabel(MainWindow)
        self.heading_2.setGeometry(QtCore.QRect(70, 60, 601, 16))
        self.heading_2.setFont(font)
        self.heading_2.setAlignment(QtCore.Qt.AlignCenter)
        self.heading_2.setObjectName("heading_2")
        self.verticalLayoutWidget = QtWidgets.QWidget(MainWindow)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(30, 140, 51, 121))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.numbers = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.numbers.setContentsMargins(0, 0, 0, 0)
        self.numbers.setSpacing(0)
        self.numbers.setObjectName("numbers")
        self.number_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.number_2.setFont(font)
        self.number_2.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-left-radius: 20px;")
        self.number_2.setObjectName("number_2")
        self.numbers.addWidget(self.number_2)
        self.number_1 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.number_1.setFont(font)
        self.number_1.setStyleSheet("background-color: #b0dedc;")
        self.number_1.setObjectName("number_1")
        self.numbers.addWidget(self.number_1)
        self.number_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.number_3.setFont(font)
        self.number_3.setStyleSheet("background-color: #c3e7e3;\n"
"border-bottom-left-radius: 20px;")
        self.number_3.setObjectName("number_3")
        self.numbers.addWidget(self.number_3)
        self.lineEdit = QtWidgets.QLineEdit(MainWindow)
        self.lineEdit.setGeometry(QtCore.QRect(30, 90, 621, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lineEdit.setFont(font)
        self.lineEdit.setStyleSheet("background-color: white;")
        self.lineEdit.setObjectName("lineEdit")
        self.search_btn = QtWidgets.QPushButton(MainWindow, clicked=self.searchBtnClicked)
        self.search_btn.setGeometry(QtCore.QRect(660, 90, 51, 41))
        self.search_btn.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\images/knopka_poiska2 (1).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.search_btn.setIcon(icon)
        self.search_btn.setIconSize(QtCore.QSize(40, 40))
        self.search_btn.setObjectName("search_btn")
        self.search_btn.setStyleSheet(
                                      "QPushButton {\n"
"border-color: black;"
"border-style: inset;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    background: rgba(0, 0, 0, 0.1);\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.5);\n"
"}\n"
"\n")
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(MainWindow)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(80, 140, 521, 121))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.titles = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.titles.setContentsMargins(0, 0, 0, 0)
        self.titles.setSpacing(0)
        self.titles.setObjectName("titles")
        self.title_1 = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.title_1.setStyleSheet("background-color: #c3e7e3;")
        self.title_1.setText("")
        self.title_1.setObjectName("title_1")
        self.titles.addWidget(self.title_1)
        self.title_2 = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.title_2.setStyleSheet("background-color: #b0dedc;")
        self.title_2.setText("")
        self.title_2.setObjectName("title_2")
        self.titles.addWidget(self.title_2)
        self.title_3 = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.title_3.setStyleSheet("background-color: #c3e7e3;")
        self.title_3.setText("")
        self.title_3.setObjectName("title_3")
        self.titles.addWidget(self.title_3)
        self.verticalLayoutWidget_3 = QtWidgets.QWidget(MainWindow)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(600, 140, 51, 121))
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.play_labels = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_3)
        self.play_labels.setContentsMargins(0, 0, 0, 0)
        self.play_labels.setSpacing(0)
        self.play_labels.setObjectName("play_labels")
        self.play_label_1 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.play_label_1.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-right-radius: 20px;")
        self.play_label_1.setText("")
        self.play_label_1.setObjectName("play_label_1")
        self.play_labels.addWidget(self.play_label_1)
        self.play_label_2 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.play_label_2.setStyleSheet("background-color: #b0dedc;")
        self.play_label_2.setText("")
        self.play_label_2.setObjectName("play_label_2")
        self.play_labels.addWidget(self.play_label_2)
        self.play_label_3 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.play_label_3.setStyleSheet("background-color: #c3e7e3;\n"
"border-bottom-right-radius: 20px;")
        self.play_label_3.setText("")
        self.play_label_3.setObjectName("play_label_3")
        self.play_labels.addWidget(self.play_label_3)
        self.verticalLayoutWidget_4 = QtWidgets.QWidget(MainWindow)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(660, 140, 51, 121))
        self.verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")
        self.check_labels = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_4)
        self.check_labels.setContentsMargins(0, 0, 0, 0)
        self.check_labels.setSpacing(0)
        self.check_labels.setObjectName("check_labels")
        self.check_label_1 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        self.check_label_1.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-right-radius: 20px;")
        self.check_label_1.setText("")
        self.check_label_1.setObjectName("check_label_1")
        self.check_labels.addWidget(self.check_label_1)
        self.check_label_2 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        self.check_label_2.setStyleSheet("background-color: #b0dedc;")
        self.check_label_2.setText("")
        self.check_label_2.setObjectName("check_label_2")
        self.check_labels.addWidget(self.check_label_2)
        self.check_label_3 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        self.check_label_3.setStyleSheet("background-color: #c3e7e3;\n"
"border-bottom-right-radius: 20px;")
        self.check_label_3.setText("")
        self.check_label_3.setObjectName("check_label_3")
        self.check_labels.addWidget(self.check_label_3)
        self.prev_page_btn = QtWidgets.QPushButton(MainWindow, clicked=self.prevPageBtnClicked)
        self.prev_page_btn.setGeometry(QtCore.QRect(0, 140, 20, 121))
        self.prev_page_btn.setStyleSheet("background-color: #b0dedc;")
        self.prev_page_btn.setObjectName("prev_page_btn")
        self.prev_page_btn.setStyleSheet("QPushButton {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: inset;"
                                         "      border-width: 1px;"
                                         "      border-bottom-left-radius: 13px;"
                                         "      border-top-left-radius: 13px;"
                                         "}\n"
                                         "QPushButton:pressed {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: outset;"
                                         "}\n")
        self.next_page_btn = QtWidgets.QPushButton(MainWindow, clicked=self.nextPageBtnClicked)
        self.next_page_btn.setGeometry(QtCore.QRect(720, 140, 20, 121))
        self.next_page_btn.setStyleSheet("background-color: #b0dedc;")
        self.next_page_btn.setObjectName("next_page_btn")
        self.next_page_btn.setStyleSheet("QPushButton {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: inset;"
                                         "      border-width: 1px;"
                                         "      border-bottom-right-radius: 13px;"
                                         "      border-top-right-radius: 13px;"
                                         "}\n"
                                         "QPushButton:pressed {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: outset;"
                                         "}\n")
        self.play_btn_1 = QtWidgets.QPushButton(MainWindow, clicked=self.playBtnClicked)
        self.play_btn_1.setGeometry(QtCore.QRect(610, 145, 31, 31))
        self.play_btn_1.setStyleSheet("background-color: #c3e7e3;")
        self.play_btn_1.setText("")
        self.play_btn_1.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(".\\images/knopka_pley3.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.play_btn_1.setIcon(icon1)
        self.play_btn_1.setObjectName("play_btn_1")
        self.play_btn_2 = QtWidgets.QPushButton(MainWindow, clicked=self.playBtnClicked)
        self.play_btn_2.setGeometry(QtCore.QRect(610, 185, 31, 31))
        self.play_btn_2.setStyleSheet("background-color: #b0dedc;")
        self.play_btn_2.setText("")
        self.play_btn_2.setIcon(icon1)
        self.play_btn_2.setObjectName("play_btn_2")
        self.play_btn_2.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_3 = QtWidgets.QPushButton(MainWindow, clicked=self.playBtnClicked)
        self.play_btn_3.setGeometry(QtCore.QRect(610, 225, 31, 31))
        self.play_btn_3.setStyleSheet("background-color: #c3e7e3;")
        self.play_btn_3.setText("")
        self.play_btn_3.setIcon(icon1)
        self.play_btn_3.setObjectName("play_btn_3")
        self.play_btn_3.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.check_btn_1 = QtWidgets.QPushButton(MainWindow, clicked=self.checkBtnClicked)
        self.check_btn_1.setGeometry(QtCore.QRect(670, 145, 31, 31))
        self.check_btn_1.setStyleSheet("background-color: #c3e7e3;")
        self.check_btn_1.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(".\\images/knopka2.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.check_btn_1.setIcon(icon2)
        self.check_btn_1.setObjectName("check_btn_1")
        self.check_btn_1.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_2 = QtWidgets.QPushButton(MainWindow, clicked=self.checkBtnClicked)
        self.check_btn_2.setGeometry(QtCore.QRect(670, 185, 31, 31))
        self.check_btn_2.setStyleSheet("background-color: #b0dedc;")
        self.check_btn_2.setText("")
        self.check_btn_2.setIcon(icon2)
        self.check_btn_2.setObjectName("check_btn_2")
        self.check_btn_2.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_3 = QtWidgets.QPushButton(MainWindow, clicked=self.checkBtnClicked)
        self.check_btn_3.setGeometry(QtCore.QRect(670, 225, 31, 31))
        self.check_btn_3.setStyleSheet("background-color: #c3e7e3;")
        self.check_btn_3.setText("")
        self.check_btn_3.setIcon(icon2)
        self.check_btn_3.setObjectName("check_btn_3")
        self.check_btn_3.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.info = QtWidgets.QLabel(MainWindow)
        self.info.setGeometry(QtCore.QRect(30, 270, 681, 20))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.info.setFont(font)
        self.info.setAlignment(QtCore.Qt.AlignCenter)
        self.info.setObjectName("info")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def searchBtnClicked(self):
        self.info.setText(self.lang_words['searchBtnClicked'])    

    def playBtnClicked(self):
        self.info.setText(self.lang_words['playBtnClicked'])

    def checkBtnClicked(self):
        self.info.setText(self.lang_words['checkBtnClicked'])

    def prevPageBtnClicked(self):
        self.info.setText(self.lang_words['prevPageBtnClicked'])

    def nextPageBtnClicked(self):
        self.info.setText(self.lang_words['nextPageBtnClicked'])

    def setLang(self):
        self.config.read("settings.ini")
        self.lang_words = dict()

        if self.config.get('GLOBAL', 'LANG') == 'English':
            self.lang_words['title'] = 'Help'
            self.lang_words['heading_1'] = 'Once you are here, you need help in mastering our simple interface'
            self.lang_words['heading_2'] = 'Let\'s analyze the main components of our interface'
            self.lang_words['lineEdit'] = 'Search string'
            self.lang_words['info'] = 'Click on any button to find out what it does'
            self.lang_words[
                'searchBtnClicked'] = 'When you enter a query and click on the button, a search for songs occurs'
            self.lang_words['playBtnClicked'] = 'When you click this button the selected track is played'
            self.lang_words[
                'checkBtnClicked'] = 'Activate the checkbox next to each desired song to mark it for download'
            self.lang_words['prevPageBtnClicked'] = 'Clicking here takes you to the previous page of the list'
            self.lang_words['nextPageBtnClicked'] = 'Clicking here takes you to the next page in the list'
        else:
            self.lang_words['title'] = 'Справка'
            self.lang_words['heading_1'] = 'Раз вы сюда попали, вам нужна помощь в освоении нашего простого интерфейса'
            self.lang_words['heading_2'] = 'Разберем основные составляющие нашего интерфейса'
            self.lang_words['lineEdit'] = 'Строка поиска'
            self.lang_words['info'] = 'Нажмите на любую кнопку, чтобы узнать, что она делает'
            self.lang_words['searchBtnClicked'] = 'Когда вы вводите запрос и нажимаете на кнопку, происходит поиск песен'
            self.lang_words['playBtnClicked'] = 'При нажатии на эту кнопку воспроизводится выбранный трек'
            self.lang_words['checkBtnClicked'] = 'Активируйте чекбокс напротив каждой нужной песни,чтобы отметить для скачивания'
            self.lang_words['prevPageBtnClicked'] = 'При нажатии сюда вы переходите на прошлую страницу списка'
            self.lang_words['nextPageBtnClicked'] = 'При нажатии сюда вы переходите на следующую страницу списка'

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", self.lang_words['title']))
        self.heading_1.setText(_translate("MainWindow", self.lang_words['heading_1']))
        self.heading_2.setText(_translate("MainWindow", self.lang_words['heading_2']))
        self.number_2.setText(_translate("MainWindow", "      1"))
        self.number_1.setText(_translate("MainWindow", "       2"))
        self.number_3.setText(_translate("MainWindow", "       3"))
        self.lineEdit.setText(_translate("MainWindow", self.lang_words['lineEdit']))
        self.prev_page_btn.setText(_translate("MainWindow", "<<"))
        self.next_page_btn.setText(_translate("MainWindow", ">>"))
        self.info.setText(_translate("MainWindow", self.lang_words['info']))
