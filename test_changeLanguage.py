import unittest
import main

import sys
import time
import re
import os
import shutil
import urllib
import urllib.error

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent

from main_window import Ui_MainWindow
from about_us import AboutUsWindow
from settings import SettingsWindow
from parser1 import Parser

import requests
import parser1
import threading
import configparser

class testChangePath(unittest.TestCase):

    def setUp(self):
        self.app = QtWidgets.QApplication([])
        self.application = main.mywindow()
        self.application.settings_window = SettingsWindow(self.application.config)

    def test_settings_change_language_directly(self):
        self.cur_language = self.application.config.get('GLOBAL', 'LANG')
        self.application.config.read("settings.ini")
        if self.cur_language == "Russian":
            self.application.config.set("GLOBAL", "LANG", "English")
        else:
            self.application.config.set("GLOBAL", "LANG", "Russian")
        self.application.config.write(open("settings.ini", "w"))
        self.assertNotEqual(self.cur_language, self.application.config.get('GLOBAL', 'LANG'))



    def test_renamed_label(self):
        self.application_cur_label_name = self.application.settings_window.label.text()
        self.cur_language = self.application.config.get('GLOBAL', 'LANG')
        self.application.config.read("settings.ini")
        if self.cur_language == "Russian":
            self.application.config.set("GLOBAL", "LANG", "English")
        else:
            self.application.config.set("GLOBAL", "LANG", "Russian")
        self.application.config.write(open("settings.ini", "w"))
        self.application.settings_window.setLang()
        self.application.config.read("settings.ini")
        self.application.settings_window.retranslateUi(self.application)
        self.assertNotEqual(self.application_cur_label_name, self.application.settings_window.label.text())

    @classmethod
    def tearDownClass(cls):
        pass

if __name__ == '__main__':
    unittest.main()