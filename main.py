import sys
import time
import re
import os
import shutil
import urllib
import urllib.error

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent

from main_window import Ui_MainWindow
from about_us import AboutUsWindow
from settings import SettingsWindow
from help import HelpWindow
from parser1 import Parser

import requests
import parser1
import threading
import configparser


class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(mywindow, self).__init__()
        self.config = configparser.ConfigParser()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self, self.config)

        self.SONG_COUNT = int(self.config.get('GLOBAL', 'QUANTITY_TRACKS'))
        if self.SONG_COUNT == 0:
            self.SONG_COUNT = 1
        self.current_page = 1
        self.MIN_PAGE = 1
        self.MAX_PAGE = self.SONG_COUNT // 10
        if self.MAX_PAGE == 0:
            self.MAX_PAGE = 1
        elif self.SONG_COUNT % 10 != 0:
            self.MAX_PAGE += 1
        self.song_list = list()
        self.thread_list = []
        self.pars = Parser('-', self.SONG_COUNT)
        self.pars = None
        self.current_track = 1
        self.program_state = False
        self.player_state = False

        self.download_path = self.config.get('GLOBAL', 'PATH')
        if not self.download_path:
            self.download_path = '.\\download'

        self.check_btn_1_state = False
        self.check_btn_2_state = False
        self.check_btn_3_state = False
        self.check_btn_4_state = False
        self.check_btn_5_state = False
        self.check_btn_6_state = False
        self.check_btn_7_state = False
        self.check_btn_8_state = False
        self.check_btn_9_state = False
        self.check_btn_10_state = False

        self.setUI()

    def setUI(self):
        self.playlist = QMediaPlaylist()

        self.player = QMediaPlayer()
        self.player.setVolume(80)
        self.player.setPlaylist(self.playlist)

        self.le_completer = QtWidgets.QCompleter(self)
        self.le_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.setHistoryCompleter()

        self.icon_pause = QtGui.QIcon()
        self.icon_pause.addPixmap(QtGui.QPixmap("images/icons8-pauza-v-kruzhke-32.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.icon_play = QtGui.QIcon()
        self.icon_play.addPixmap(QtGui.QPixmap('images/knopka_pley3.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.icon_n_checked = QtGui.QIcon()
        self.icon_n_checked.addPixmap(QtGui.QPixmap('images/knopka2.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.icon_checked = QtGui.QIcon()
        self.icon_checked.addPixmap(QtGui.QPixmap('images/knopka_s_galochkoy2.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.ui.play_btn_1.setGeometry(QtCore.QRect(860, 80, 31, 31))
        self.ui.play_btn_2.setGeometry(QtCore.QRect(860, 118, 31, 31))
        self.ui.play_btn_3.setGeometry(QtCore.QRect(860, 158, 31, 31))
        self.ui.play_btn_4.setGeometry(QtCore.QRect(860, 197, 31, 31))
        self.ui.play_btn_5.setGeometry(QtCore.QRect(860, 236, 31, 31))
        self.ui.play_btn_6.setGeometry(QtCore.QRect(860, 275, 31, 31))
        self.ui.play_btn_7.setGeometry(QtCore.QRect(860, 314, 31, 31))
        self.ui.play_btn_8.setGeometry(QtCore.QRect(860, 353, 31, 31))
        self.ui.play_btn_9.setGeometry(QtCore.QRect(860, 392, 31, 31))
        self.ui.play_btn_10.setGeometry(QtCore.QRect(860, 431, 31, 31))

        self.ui.check_btn_1.setGeometry(QtCore.QRect(920, 80, 31, 31))
        self.ui.check_btn_2.setGeometry(QtCore.QRect(920, 118, 31, 31))
        self.ui.check_btn_3.setGeometry(QtCore.QRect(920, 158, 31, 31))
        self.ui.check_btn_4.setGeometry(QtCore.QRect(920, 197, 31, 31))
        self.ui.check_btn_5.setGeometry(QtCore.QRect(920, 236, 31, 31))
        self.ui.check_btn_6.setGeometry(QtCore.QRect(920, 275, 31, 31))
        self.ui.check_btn_7.setGeometry(QtCore.QRect(920, 314, 31, 31))
        self.ui.check_btn_8.setGeometry(QtCore.QRect(920, 353, 31, 31))
        self.ui.check_btn_9.setGeometry(QtCore.QRect(920, 392, 31, 31))
        self.ui.check_btn_10.setGeometry(QtCore.QRect(920, 431, 31, 31))

        self.ui.play_btn_1.clicked.connect(self.playClicked_1)
        self.ui.play_btn_2.clicked.connect(self.playClicked_2)
        self.ui.play_btn_3.clicked.connect(self.playClicked_3)
        self.ui.play_btn_4.clicked.connect(self.playClicked_4)
        self.ui.play_btn_5.clicked.connect(self.playClicked_5)
        self.ui.play_btn_6.clicked.connect(self.playClicked_6)
        self.ui.play_btn_7.clicked.connect(self.playClicked_7)
        self.ui.play_btn_8.clicked.connect(self.playClicked_8)
        self.ui.play_btn_9.clicked.connect(self.playClicked_9)
        self.ui.play_btn_10.clicked.connect(self.playClicked_10)

        self.ui.check_btn_1.clicked.connect(self.checkClicked_1)
        self.ui.check_btn_2.clicked.connect(self.checkClicked_2)
        self.ui.check_btn_3.clicked.connect(self.checkClicked_3)
        self.ui.check_btn_4.clicked.connect(self.checkClicked_4)
        self.ui.check_btn_5.clicked.connect(self.checkClicked_5)
        self.ui.check_btn_6.clicked.connect(self.checkClicked_6)
        self.ui.check_btn_7.clicked.connect(self.checkClicked_7)
        self.ui.check_btn_8.clicked.connect(self.checkClicked_8)
        self.ui.check_btn_9.clicked.connect(self.checkClicked_9)
        self.ui.check_btn_10.clicked.connect(self.checkClicked_10)

        self.ui.player_btn.clicked.connect(self.play)
        self.ui.rigthscroll_btn.clicked.connect(self.nextTrack)
        self.ui.leftscroll_btn.clicked.connect(self.previousTrack)

        self.ui.search_btn.clicked.connect(self.find)
        self.ui.download_btn.clicked.connect(self.download)

        self.ui.prev_page_btn.clicked.connect(self.prevPage)
        self.ui.next_page_btn.clicked.connect(self.nextPage)

        self.ui.settings.triggered.connect(self.settingsClicked)
        self.ui.about_us.triggered.connect(self.aboutUsClicked)
        self.ui.help.triggered.connect(self.helpClicked)

        self.ui.qsl.sliderMoved[int].connect(self.setPlayPosition)
        self.player_timer = QtCore.QTimer(self)
        self.player_timer.timeout.connect(self.playProccess)
        self.player_timer.start(1000)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # !!!!  Код после этой метки должен быть всегда в конце функции  !!!!
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        self.ui.label_20.setText("     1")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 1:
            self.ui.play_btn_2.setVisible(False)
            self.ui.check_btn_2.setVisible(False)
            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_11.setText("      2")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 2:
            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_4.setText("      3")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 3:
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_14.setText("      4")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 4:
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_16.setText("      5")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 5:
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_13.setText("      6")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 6:
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_18.setText("      7")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 7:
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_8.setText("      8")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 8:
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_2.setText("      9")
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 9:
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_6.setText("    10")

    def playCheck(self):
        if self.program_state:
            if self.player_state:
                self.ui.player_btn.setIcon(self.icon_play)
                self.player_state = False
                return True
            else:
                self.ui.player_btn.setIcon(self.icon_pause)
                self.player_state = True
                return False

    def playClicked_1(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(0)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 1
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])

    def playClicked_2(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(1)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 2
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_3(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(2)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 3
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_4(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(3)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 4
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_5(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(4)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 5
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_6(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(5)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 6
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_7(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(6)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 7
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_8(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(7)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 8
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_9(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(8)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 9
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def playClicked_10(self):
        if self.program_state:
            self.player.stop()
            self.playlist.setCurrentIndex(9)
            self.player.play()
            self.current_track = (self.current_page - 1) * 10 + 10
            self.ui.player_btn.setIcon(self.icon_pause)
            self.ui.qsl.setValue(0)
            self.player_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])

    def checkClicked_1(self):
        self.check_btn_1_state = not self.check_btn_1_state
        if self.check_btn_1_state:
            self.ui.check_btn_1.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_1.setIcon(self.icon_n_checked)
    
    def checkClicked_2(self):
        self.check_btn_2_state = not self.check_btn_2_state
        if self.check_btn_2_state:
            self.ui.check_btn_2.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_2.setIcon(self.icon_n_checked)

    def checkClicked_3(self):
        self.check_btn_3_state = not self.check_btn_3_state
        if self.check_btn_3_state:
            self.ui.check_btn_3.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_3.setIcon(self.icon_n_checked)
    
    def checkClicked_4(self):
        self.check_btn_4_state = not self.check_btn_4_state
        if self.check_btn_4_state:
            self.ui.check_btn_4.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_4.setIcon(self.icon_n_checked)
    
    def checkClicked_5(self):
        self.check_btn_5_state = not self.check_btn_5_state
        if self.check_btn_5_state:
            self.ui.check_btn_5.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_5.setIcon(self.icon_n_checked)
    
    def checkClicked_6(self):
        self.check_btn_6_state = not self.check_btn_6_state
        if self.check_btn_6_state:
            self.ui.check_btn_6.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_6.setIcon(self.icon_n_checked)
    
    def checkClicked_7(self):
        self.check_btn_7_state = not self.check_btn_7_state
        if self.check_btn_7_state:
            self.ui.check_btn_7.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_7.setIcon(self.icon_n_checked)
    
    def checkClicked_8(self):
        self.check_btn_8_state = not self.check_btn_8_state
        if self.check_btn_8_state:
            self.ui.check_btn_8.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_8.setIcon(self.icon_n_checked)
    
    def checkClicked_9(self):
        self.check_btn_9_state = not self.check_btn_9_state
        if self.check_btn_9_state:
            self.ui.check_btn_9.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_9.setIcon(self.icon_n_checked)

    def checkClicked_10(self):
        self.check_btn_10_state = not self.check_btn_10_state
        if self.check_btn_10_state:
            self.ui.check_btn_10.setIcon(self.icon_checked)
        else:
            self.ui.check_btn_10.setIcon(self.icon_n_checked)

    def play(self):
        if self.playCheck():
            self.player.pause()
        else:
            self.player.play()

    def nextTrack(self):
        if self.player_state:
            self.ui.qsl.setValue(0)
            self.current_track += 1
            self.playlist.next()
            
        if self.current_track > self.SONG_COUNT:
            self.current_track = 1
            self.player.stop()
            self.playlist.setCurrentIndex(0)
            self.playCheck()
        
        if self.program_state:
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])
    
    def previousTrack(self):
        if self.player_state:
            self.ui.qsl.setValue(0)
            self.current_track -= 1
            self.playlist.previous()

        if self.current_track < 1:
            self.player.stop()
            self.playlist.setCurrentIndex(0)
            self.playCheck()
        
        if self.program_state:
            self.ui.cur_tr_name.setText(self.song_list[self.current_track]['name'])

    def find(self):
        if self.ui.search_line.text() != '':
            self.ui.label_21.setText("")
            self.ui.label_12.setText("")
            self.ui.label_5.setText("")
            self.ui.label_15.setText("")
            self.ui.label_3.setText("")
            self.ui.label_19.setText("")
            self.ui.label_10.setText("")
            self.ui.label_17.setText("")
            self.ui.label_9.setText("")
            self.ui.label_7.setText("")

            track_name = self.ui.search_line.text()
            was_added = False
            if os.path.exists('history.txt'):
                file = open('history.txt', 'r+')
                for s in file:
                    if str(s) == (track_name + '\n'):
                        was_added = True
                if not was_added:
                    file.write(track_name + '\n')
                file.close()
                        
            self.setHistoryCompleter()

            self.song_name = re.sub(' ', '+', self.ui.search_line.text())
            self.thread1 = parser1.Parser(self.song_name, self.SONG_COUNT)
            self.thread1.start()
            self.thread1.join()

            for song in self.thread1.song_list:
                self.song_list.append(song)
            
            self.playlist.clear()

            for song in self.song_list:
                # self.playlist.addMedia(QMediaContent(QtCore.QUrl(song['url'])))
                self.playlist.addMedia(QMediaContent(QtCore.QUrl(str(song['url']))))
            self.playlist.setCurrentIndex(0)
            self.program_state = True
            self.ui.cur_tr_name.setText(str(self.current_track) + " - " + self.song_list[self.current_track - 1]['name'])

            self.ui.search_line.setText(None)
            self.ui.label_21.setText(f'{self.thread1.song_list[0].get("artist")} - {self.thread1.song_list[0].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 1:
                return
            self.ui.label_12.setText(f'{self.thread1.song_list[1].get("artist")} - {self.thread1.song_list[1].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 2:
                return
            self.ui.label_5.setText(f'{self.thread1.song_list[2].get("artist")} - {self.thread1.song_list[2].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 3:
                return
            self.ui.label_15.setText(f'{self.thread1.song_list[3].get("artist")} - {self.thread1.song_list[3].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 4:
                return
            self.ui.label_3.setText(f'{self.thread1.song_list[4].get("artist")} - {self.thread1.song_list[4].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 5:
                return
            self.ui.label_19.setText(f'{self.thread1.song_list[5].get("artist")} - {self.thread1.song_list[5].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 6:
                return
            self.ui.label_10.setText(f'{self.thread1.song_list[6].get("artist")} - {self.thread1.song_list[6].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 7:
                return
            self.ui.label_17.setText(f'{self.thread1.song_list[7].get("artist")} - {self.thread1.song_list[7].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 8:
                return
            self.ui.label_9.setText(f'{self.thread1.song_list[8].get("artist")} - {self.thread1.song_list[8].get("name")}')
            if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 9:
                return
            self.ui.label_7.setText(f'{self.thread1.song_list[9].get("artist")} - {self.thread1.song_list[9].get("name")}')

    def download(self):
        if not self.program_state:
            return
        if self.check_btn_1_state:
            self.checkClicked_1()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 1:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                     args=(self.thread1.song_list, ((self.current_page-1) * 10 + 0), self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_21.setText("Invalid URL!")
        if self.check_btn_2_state:
            self.checkClicked_2()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 2:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                     args=(self.thread1.song_list, (self.current_page-1) * 10 + 1, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_5.setText("Invalid URL!")
        if self.check_btn_3_state:
            self.checkClicked_3()
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 2, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_5.setText("Invalid URL!")
        if self.check_btn_4_state:
            self.checkClicked_4()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 4:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 3, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_15.setText("Invalid URL!")
        if self.check_btn_5_state:
            self.checkClicked_5()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 5:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 4, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_3.setText("Invalid URL!")
        if self.check_btn_6_state:
            self.checkClicked_6()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 6:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 5, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_19.setText("Invalid URL!")
        if self.check_btn_7_state:
            self.checkClicked_7()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 7:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 6, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_10.setText("Invalid URL!")
        if self.check_btn_8_state:
            self.checkClicked_8()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 8:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 7, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_17.setText("Invalid URL!")
        if self.check_btn_9_state:
            self.checkClicked_9()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 9:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 8, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_9.setText("Invalid URL!")
        if self.check_btn_10_state:
            self.checkClicked_10()
            # if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 0:
            try:
                t = threading.Thread(target=self.thread1.download_song,
                                    args=(self.thread1.song_list, (self.current_page-1) * 10 + 9, self.download_path))
                self.thread_list.append(t)
                t.start()
            except urllib.error.URLError:
                self.ui.label_7.setText("Invalid URL!")

        
    def settingsClicked(self):
        self.settings_window = SettingsWindow(self.config)
        self.settings_window.show()
        self.settings_window.quant_tracks.setValue(self.SONG_COUNT)
        self.settings_window.path_btn.clicked.connect(self.changePath)
        self.settings_window.chl_comp.clicked.connect(self.changeLanguage)
        self.settings_window.qtr_comp.clicked.connect(self.changeQuantTracks)


    def changePath(self):
        self.config.read("settings.ini")
        if self.download_path != self.config.get('GLOBAL', 'PATH'):
            self.download_path = self.config.get('GLOBAL', 'PATH')


    def changeLanguage(self):
        self.settings_window.setLang()
        self.ui.setLang()
        self.ui.retranslateUi(self)
        self.settings_window.retranslateUi(self.settings_window)


    def changeQuantTracks(self):
        self.config.read("settings.ini")
        self.SONG_COUNT = int(self.config.get('GLOBAL', 'QUANTITY_TRACKS'))
        if self.SONG_COUNT == 0:
            self.SONG_COUNT = 1

        self.current_page = 1
        self.MIN_PAGE = 1
        self.MAX_PAGE = self.SONG_COUNT // 10
        if self.MAX_PAGE == 0:
            self.MAX_PAGE = 1
        elif self.SONG_COUNT % 10 != 0:
            self.MAX_PAGE += 1

        self.program_state = False
        self.player_state = False

        self.player.stop()
        self.playlist.clear()
        self.thread_list.clear()
        self.song_list.clear()
        self.current_track = 1

        self.ui.player_btn.setIcon(self.icon_play)
        self.ui.qsl.setValue(0)
        self.ui.search_line.setText("")

        self.ui.retranslateUi(self)

        self.check_btn_1_state = False
        self.check_btn_2_state = False
        self.check_btn_3_state = False
        self.check_btn_4_state = False
        self.check_btn_5_state = False
        self.check_btn_6_state = False
        self.check_btn_7_state = False
        self.check_btn_8_state = False
        self.check_btn_9_state = False
        self.check_btn_10_state = False

        self.ui.check_btn_1.setIcon(self.icon_n_checked)
        self.ui.check_btn_2.setIcon(self.icon_n_checked)
        self.ui.check_btn_3.setIcon(self.icon_n_checked)
        self.ui.check_btn_4.setIcon(self.icon_n_checked)
        self.ui.check_btn_5.setIcon(self.icon_n_checked)
        self.ui.check_btn_6.setIcon(self.icon_n_checked)
        self.ui.check_btn_7.setIcon(self.icon_n_checked)
        self.ui.check_btn_8.setIcon(self.icon_n_checked)
        self.ui.check_btn_9.setIcon(self.icon_n_checked)
        self.ui.check_btn_10.setIcon(self.icon_n_checked)

        self.ui.label_20.setText("")
        self.ui.label_11.setText("")
        self.ui.label_4.setText("")
        self.ui.label_14.setText("")
        self.ui.label_16.setText("")
        self.ui.label_13.setText("")
        self.ui.label_18.setText("")
        self.ui.label_8.setText("")
        self.ui.label_2.setText("")
        self.ui.label_6.setText("")

        self.ui.label_21.setText("")
        self.ui.label_12.setText("")
        self.ui.label_5.setText("")
        self.ui.label_15.setText("")
        self.ui.label_3.setText("")
        self.ui.label_19.setText("")
        self.ui.label_10.setText("")
        self.ui.label_17.setText("")
        self.ui.label_9.setText("")
        self.ui.label_7.setText("")

        self.ui.label_20.setText("     1")
        self.ui.play_btn_1.setVisible(True)
        self.ui.check_btn_1.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 1:
            self.ui.play_btn_2.setVisible(False)
            self.ui.check_btn_2.setVisible(False)
            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_11.setText("      2")
        self.ui.play_btn_2.setVisible(True)
        self.ui.check_btn_2.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 2:
            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_4.setText("      3")
        self.ui.play_btn_3.setVisible(True)
        self.ui.check_btn_3.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 3:
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_14.setText("      4")
        self.ui.play_btn_4.setVisible(True)
        self.ui.check_btn_4.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 4:
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_16.setText("      5")
        self.ui.play_btn_5.setVisible(True)
        self.ui.check_btn_5.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 5:
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_13.setText("      6")
        self.ui.play_btn_6.setVisible(True)
        self.ui.check_btn_6.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 6:
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_18.setText("      7")
        self.ui.play_btn_7.setVisible(True)
        self.ui.check_btn_7.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 7:
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_8.setText("      8")
        self.ui.play_btn_8.setVisible(True)
        self.ui.check_btn_8.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 8:
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_2.setText("      9")
        self.ui.play_btn_9.setVisible(True)
        self.ui.check_btn_9.setVisible(True)
        if self.SONG_COUNT < 10 and self.SONG_COUNT % 10 == 9:
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_6.setText("    10")
        self.ui.play_btn_10.setVisible(True)
        self.ui.check_btn_10.setVisible(True)


    def aboutUsClicked(self):
        self.about_us_window = AboutUsWindow(self.config)
        self.about_us_window.show()


    def helpClicked(self):
        self.help_window = HelpWindow(self.config)
        self.help_window.show()


    def setPlayPosition(self):
        self.player.stop()
        self.player.setPosition(self.ui.qsl.value())
        self.player.play()

    def playProccess(self):
        if self.player_state:
            self.ui.qsl.setMinimum(0)
            self.ui.qsl.setMaximum(self.player.duration())
            self.ui.qsl.setValue(self.ui.qsl.value() + 1000)

        self.ui.timer_start.setText(time.strftime('%M:%S',time.localtime(self.player.position() / 1000)))
        self.ui.timer_end.setText(time.strftime('%M:%S', time.localtime(self.player.duration() / 1000)))

        if self.player.position() == self.player.duration() and self.player.duration != 0 and self.player_state:
            self.nextTrack()

    def setHistoryCompleter(self):
        if os.path.exists('history.txt'):
            with open('history.txt', 'r') as file:
                history_list = [line.strip() for line in file]
                q_history_list = QtCore.QStringListModel()
                q_history_list.setStringList(history_list)
        self.le_completer.setModel(q_history_list)
        self.ui.search_line.setCompleter(self.le_completer)
    
    def prevPage(self):
        if not self.program_state:
            return
        if self.current_page == self.MIN_PAGE:
            return
        self.current_page -= 1

        self.ui.play_btn_2.setVisible(True)
        self.ui.check_btn_2.setVisible(True)
        self.ui.play_btn_3.setVisible(True)
        self.ui.check_btn_3.setVisible(True)
        self.ui.play_btn_4.setVisible(True)
        self.ui.check_btn_4.setVisible(True)
        self.ui.play_btn_5.setVisible(True)
        self.ui.check_btn_5.setVisible(True)
        self.ui.play_btn_6.setVisible(True)
        self.ui.check_btn_6.setVisible(True)
        self.ui.play_btn_7.setVisible(True)
        self.ui.check_btn_7.setVisible(True)
        self.ui.play_btn_8.setVisible(True)
        self.ui.check_btn_8.setVisible(True)
        self.ui.play_btn_9.setVisible(True)
        self.ui.check_btn_9.setVisible(True)
        self.ui.play_btn_10.setVisible(True)
        self.ui.check_btn_10.setVisible(True)

        self.ui.label_21.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 0].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 0].get("name")}')
        self.ui.label_20.setText("    " + str((self.current_page-1) * 10 + 1))

        self.ui.label_12.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 1].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 1].get("name")}')
        self.ui.label_11.setText("     " + str((self.current_page-1) * 10 + 2))

        self.ui.label_12.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 1].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 1].get("name")}')
        self.ui.label_11.setText("     " + str((self.current_page-1) * 10 + 2))

        self.ui.label_5.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 2].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 2].get("name")}')
        self.ui.label_4.setText("     " + str((self.current_page-1) * 10 + 3))

        self.ui.label_15.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 3].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 3].get("name")}')
        self.ui.label_14.setText("     " + str((self.current_page-1) * 10 + 4))

        self.ui.label_3.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 4].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 4].get("name")}')
        self.ui.label_16.setText("     " + str((self.current_page-1) * 10 + 5))

        self.ui.label_19.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 5].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 5].get("name")}')
        self.ui.label_13.setText("     " + str((self.current_page-1) * 10 + 6))
        
        self.ui.label_10.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 6].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 6].get("name")}')
        self.ui.label_18.setText("     " + str((self.current_page-1) * 10 + 7))
        
        self.ui.label_17.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 7].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 7].get("name")}')
        self.ui.label_8.setText("     " + str((self.current_page-1) * 10 + 8))

        self.ui.label_9.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 8].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 8].get("name")}')
        self.ui.label_2.setText("     " + str((self.current_page-1) * 10 + 9))

        self.ui.label_7.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 9].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 9].get("name")}')
        self.ui.label_6.setText("   " + str((self.current_page-1) * 10 + 10))

    def nextPage(self):
        if not self.program_state:
            return
        if self.current_page == self.MAX_PAGE:
            return
        self.current_page += 1

        self.ui.label_21.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 0].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 0].get("name")}')
        self.ui.label_20.setText("    " + str((self.current_page-1) * 10 + 1))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 1:
            self.ui.label_12.setText("")
            self.ui.label_11.setText("")
            self.ui.label_5.setText("")
            self.ui.label_4.setText("")
            self.ui.label_15.setText("")
            self.ui.label_14.setText("")
            self.ui.label_3.setText("")
            self.ui.label_16.setText("")
            self.ui.label_19.setText("")
            self.ui.label_13.setText("")
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_2.setVisible(False)
            self.ui.check_btn_2.setVisible(False)
            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_12.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 1].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 1].get("name")}')
        self.ui.label_11.setText("     " + str((self.current_page-1) * 10 + 2))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 2:
            self.ui.label_5.setText("")
            self.ui.label_4.setText("")
            self.ui.label_15.setText("")
            self.ui.label_14.setText("")
            self.ui.label_3.setText("")
            self.ui.label_16.setText("")
            self.ui.label_19.setText("")
            self.ui.label_13.setText("")
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_3.setVisible(False)
            self.ui.check_btn_3.setVisible(False)
            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_5.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 2].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 2].get("name")}')
        self.ui.label_4.setText("     " + str((self.current_page-1) * 10 + 3))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 3:
            self.ui.label_15.setText("")
            self.ui.label_14.setText("")
            self.ui.label_3.setText("")
            self.ui.label_16.setText("")
            self.ui.label_19.setText("")
            self.ui.label_13.setText("")
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_4.setVisible(False)
            self.ui.check_btn_4.setVisible(False)
            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_15.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 3].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 3].get("name")}')
        self.ui.label_14.setText("     " + str((self.current_page-1) * 10 + 4))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 4:
            self.ui.label_3.setText("")
            self.ui.label_16.setText("")
            self.ui.label_19.setText("")
            self.ui.label_13.setText("")
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_5.setVisible(False)
            self.ui.check_btn_5.setVisible(False)
            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_3.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 4].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 4].get("name")}')
        self.ui.label_16.setText("     " + str((self.current_page-1) * 10 + 5))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 5:
            self.ui.label_19.setText("")
            self.ui.label_13.setText("")
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_6.setVisible(False)
            self.ui.check_btn_6.setVisible(False)
            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_19.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 5].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 5].get("name")}')
        self.ui.label_13.setText("     " + str((self.current_page-1) * 10 + 6))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 6:
            self.ui.label_10.setText("")
            self.ui.label_18.setText("")
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_7.setVisible(False)
            self.ui.check_btn_7.setVisible(False)
            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_10.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 6].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 6].get("name")}')
        self.ui.label_18.setText("     " + str((self.current_page-1) * 10 + 7))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 7:
            self.ui.label_17.setText("")
            self.ui.label_8.setText("")
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_8.setVisible(False)
            self.ui.check_btn_8.setVisible(False)
            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_17.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 7].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 7].get("name")}')
        self.ui.label_8.setText("     " + str((self.current_page-1) * 10 + 8))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 8:
            self.ui.label_9.setText("")
            self.ui.label_2.setText("")
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_9.setVisible(False)
            self.ui.check_btn_9.setVisible(False)
            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_9.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 8].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 8].get("name")}')
        self.ui.label_2.setText("     " + str((self.current_page-1) * 10 + 9))

        if self.current_page == self.MAX_PAGE and self.SONG_COUNT % 10 == 9:
            self.ui.label_7.setText("")
            self.ui.label_6.setText("")

            self.ui.play_btn_10.setVisible(False)
            self.ui.check_btn_10.setVisible(False)
            return
        self.ui.label_7.setText(f'{self.thread1.song_list[(self.current_page-1) * 10 + 9].get("artist")} - {self.thread1.song_list[(self.current_page-1) * 10 + 9].get("name")}')
        self.ui.label_6.setText("   " + str((self.current_page-1) * 10 + 10))


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    application = mywindow()
    application.show()
    sys.exit(app.exec())
