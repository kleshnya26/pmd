import site_parse as sp

import threading
from threading import Thread
import queue
import urllib.request

URL_COUNT = 10


class Parser(Thread):
    def __init__(self, song_name, song_count):
        Thread.__init__(self)
        self.song_name = song_name
        self.leftover_tracks_count = self.song_count = song_count
        self.url_count = URL_COUNT
        self.song_list = list()

    def get_song_list(self):
        """ Получение списка словарей найденных песен
        """

        parser_list = list()
        song_list = list()
        sq = queue.Queue()

        parser_list.append(sp.parser_muzfan_net.get_song_list)
        parser_list.append(sp.parser_backingtrackx_com.get_song_list)
        parser_list.append(sp.parser_drivemusic_net.get_song_list)
        parser_list.append(sp.parser_hotmo_org.get_song_list)
        parser_list.append(sp.parser_hotplayer_ru.get_song_list)
        parser_list.append(sp.parser_imusic_wiki.get_song_list)
        parser_list.append(sp.parser_mp3yoda_com.get_song_list)
        parser_list.append(sp.parser_lightradio_ru.get_song_list)
        parser_list.append(sp.parser_realrocks_ru.get_song_list)
        parser_list.append(sp.parser_mp3fan_cc.get_song_list)

        for i in range(5):
            thread_list = list()

            for j in range(self.url_count):
                thread_list.append(threading.Thread(target=parser_list[j], args=(self.song_name, i + 1, sq)))
                thread_list[j].start()
            for j in range(self.url_count):
                thread_list[j].join()

            while not sq.empty():
                elem = sq.get()
                if elem and elem not in song_list:
                    song_list.append(elem)
                    self.leftover_tracks_count -= 1
                    if self.leftover_tracks_count == 0:
                        return song_list

        return song_list

    def print_song_list(self):
        for i in range(len(self.song_list)):
            print(str(i + 1) + ') ' + str(self.song_list[i]) + '\n')

    def run(self):
        self.song_list = self.get_song_list()

    def set_song_name(self, song_name):
        self.song_name = song_name

    def download_song(self, song_list, index, download_path):
        if song_list:
            file_name = song_list[index].get('artist') + ' - ' + song_list[index].get('name') + '.mp3'
            url = song_list[index].get("url")
            urllib.request.urlretrieve(url, download_path + '\\' + file_name)
