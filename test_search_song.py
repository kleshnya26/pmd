import unittest
import main
from PyQt5 import QtWidgets

TEST_SONG_COUNT_1 = 10
TEST_SONG_COUNT_2 = 15
TEST_SONG_1 = 'Coffin dance'
TEST_SONG_2 = 'Just song'


class TestSongSearch(unittest.TestCase):

    def setUp(self):
        self.app = QtWidgets.QApplication([])
        self.application = main.mywindow()
        self.application.setUI()
        self.application.SONG_COUNT = TEST_SONG_COUNT_1

    def test_not_empty_song_10(self):
        self.application.ui.search_line.setText(TEST_SONG_1)
        self.application.find()
        self.assertEqual(len(self.application.song_list), TEST_SONG_COUNT_1)
        self.assertTrue(self.application.ui.label_21.text() and self.application.ui.label_7.text())

    def test_not_empty_song_15(self):
        self.application.SONG_COUNT = TEST_SONG_COUNT_2
        self.application.ui.search_line.setText(TEST_SONG_2)
        self.application.find()
        self.application.nextPage()
        self.assertEqual(len(self.application.song_list), TEST_SONG_COUNT_2)
        self.assertTrue(self.application.ui.label_21.text() and self.application.ui.label_3.text())

    def test_empty_song(self):
        self.application.ui.search_line.setText("")
        self.application.find()
        self.assertFalse(self.application.ui.label_21.text())


if __name__ == '__main__':
    unittest.main()
