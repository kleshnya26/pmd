from PyQt5 import QtCore, QtGui, QtWidgets


class AboutUsWindow(QtWidgets.QWidget):
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.setLang()
        self.setupUi(self)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(554, 216)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 30, 511, 121))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.title = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setObjectName("title")
        self.verticalLayout.addWidget(self.title)
        self.max = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.max.setAlignment(QtCore.Qt.AlignCenter)
        self.max.setObjectName("max")
        self.verticalLayout.addWidget(self.max)
        self.fil = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.fil.setAlignment(QtCore.Qt.AlignCenter)
        self.fil.setObjectName("fil")
        self.verticalLayout.addWidget(self.fil)
        self.vlad = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.vlad.setAlignment(QtCore.Qt.AlignCenter)
        self.vlad.setObjectName("vlad")
        self.verticalLayout.addWidget(self.vlad)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", self.lang_words['about_us']))
        self.title.setText(_translate("MainWindow", self.lang_words['introduction']))
        self.max.setText(_translate("MainWindow", self.lang_words['borodynya']))
        self.fil.setText(_translate("MainWindow", self.lang_words['zheravin']))
        self.vlad.setText(_translate("MainWindow", self.lang_words['rodyushkin']))

    def setLang(self):
        self.config.read("settings.ini")
        self.lang_words = dict()

        if self.config.get('GLOBAL', 'LANG') == 'English':
            self.lang_words['about_us'] = 'About us'
            self.lang_words['introduction'] = 'The following people tried to find a coronavirus vaccine in free music:'
            self.lang_words['borodynya'] = 'Borodynya Maxim'
            self.lang_words['zheravin'] = 'Zheravin Philip'
            self.lang_words['rodyushkin'] = 'Rodyushkin Vladislav'
        else:
            self.lang_words['about_us'] = 'О Нас'
            self.lang_words['introduction'] = 'Старались найти вакцину от коронавируса в бесплатной музыке:'
            self.lang_words['borodynya'] = 'Бородыня Максим'
            self.lang_words['zheravin'] = 'Жеравин Филипп'
            self.lang_words['rodyushkin'] = 'Родюшкин Владислав'

    print(('Hi'.encode('UTF-8')).decode('UTF-8'))
