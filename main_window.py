from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow, config):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(992, 614)
        MainWindow.setStyleSheet("background-color: #E2F9FF;")
        MainWindow.setWindowIcon(QtGui.QIcon('images/icon.jpg'))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow.setCentralWidget(self.centralwidget)

        self.config = config
        self.setLang()

        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1002, 26))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menu.setStyleSheet("background-color: #c3e7e3;\n"
                                "color: black;")
        MainWindow.setMenuBar(self.menubar)
        self.settings = QtWidgets.QAction(MainWindow)
        self.settings.setObjectName("settings")
        self.about_us = QtWidgets.QAction(MainWindow)
        self.about_us.setObjectName("about_us")
        self.help = QtWidgets.QAction(MainWindow)
        self.help.setObjectName("help")
        self.menu.addAction(self.settings)
        self.menu.addAction(self.about_us)
        self.menu.addAction(self.help)
        self.menubar.addAction(self.menu.menuAction())
        self.search_line = QtWidgets.QLineEdit(self.centralwidget)
        self.search_line.setGeometry(QtCore.QRect(10+20, 25, 871, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.search_line.setFont(font)
        self.search_line.setStyleSheet("background-color: white;")
        self.search_line.setObjectName("search_line")
        self.search_btn = QtWidgets.QPushButton(self.centralwidget)
        self.search_btn.setGeometry(QtCore.QRect(890+20, 25, 51, 41))
        self.search_btn.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\images/knopka_poiska2 (1).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.search_btn.setIcon(icon)
        self.search_btn.setIconSize(QtCore.QSize(40, 40))
        self.search_btn.setObjectName("search_btn")
        self.search_btn.setStyleSheet(
                                      "QPushButton {\n"
"border-color: black;"
"border-style: inset;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    background: rgba(0, 0, 0, 0.1);\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.5);\n"
"}\n"
"\n")

        self.prev_page_btn = QtWidgets.QPushButton(self.centralwidget)
        self.prev_page_btn.setGeometry(QtCore.QRect(5, 75, 20, 391))
        self.prev_page_btn.setStyleSheet("QPushButton {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: inset;"
                                         "      border-width: 1px;"
                                         "      border-bottom-left-radius: 13px;"
                                         "      border-top-left-radius: 13px;"
                                         "}\n"
                                         "QPushButton:pressed {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: outset;"
                                         "}\n")
        self.next_page_btn = QtWidgets.QPushButton(self.centralwidget)
        self.next_page_btn.setGeometry(QtCore.QRect(967, 75, 20, 391))
        self.next_page_btn.setStyleSheet("QPushButton {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: inset;"
                                         "      border-width: 1px;"
                                         "      border-bottom-right-radius: 13px;"
                                         "      border-top-right-radius: 13px;"
                                         "}\n"
                                         "QPushButton:pressed {\n"
                                         "      background-color: #b0dedc;"
                                         "      border-style: outset;"
                                         "}\n")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(890+20, 75, 51, 391))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_33 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_33.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-right-radius: 20px;")
        self.label_33.setText("")
        self.label_33.setObjectName("label_33")
        self.verticalLayout_3.addWidget(self.label_33)
        self.label_32 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_32.setStyleSheet("background-color: #b0dedc;")
        self.label_32.setText("")
        self.label_32.setObjectName("label_32")
        self.verticalLayout_3.addWidget(self.label_32)
        self.label_36 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_36.setStyleSheet("background-color: #c3e7e3;")
        self.label_36.setText("")
        self.label_36.setObjectName("label_36")
        self.verticalLayout_3.addWidget(self.label_36)
        self.label_40 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_40.setStyleSheet("background-color: #b0dedc;")
        self.label_40.setText("")
        self.label_40.setObjectName("label_40")
        self.verticalLayout_3.addWidget(self.label_40)
        self.label_35 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_35.setStyleSheet("background-color: #c3e7e3;")
        self.label_35.setText("")
        self.label_35.setObjectName("label_35")
        self.verticalLayout_3.addWidget(self.label_35)
        self.label_38 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_38.setStyleSheet("background-color: #b0dedc;")
        self.label_38.setText("")
        self.label_38.setObjectName("label_38")
        self.verticalLayout_3.addWidget(self.label_38)
        self.label_37 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_37.setStyleSheet("background-color: #c3e7e3;")
        self.label_37.setText("")
        self.label_37.setObjectName("label_37")
        self.verticalLayout_3.addWidget(self.label_37)
        self.label_34 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_34.setStyleSheet("background-color: #b0dedc;")
        self.label_34.setText("")
        self.label_34.setObjectName("label_34")
        self.verticalLayout_3.addWidget(self.label_34)
        self.label_39 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_39.setStyleSheet("background-color: #c3e7e3;")
        self.label_39.setText("")
        self.label_39.setObjectName("label_39")
        self.verticalLayout_3.addWidget(self.label_39)
        self.label_41 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_41.setStyleSheet("background-color: #b0dedc;\n"
"border: 1px solid #b0dedc;\n"
"border-bottom-right-radius: 20px;")
        self.label_41.setText("")
        self.label_41.setObjectName("label_41")
        self.verticalLayout_3.addWidget(self.label_41)
        self.verticalLayoutWidget_3 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(60+20, 75, 771, 391))
        self.verticalLayoutWidget_3.setObjectName("verticalLayoutWidget_3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_21 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_21.setStyleSheet("background-color: #c3e7e3;")
        self.label_21.setText("")
        self.label_21.setObjectName("label_21")
        self.verticalLayout_5.addWidget(self.label_21)
        self.label_12 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_12.setStyleSheet("background-color: #b0dedc;")
        self.label_12.setText("")
        self.label_12.setObjectName("label_12")
        self.verticalLayout_5.addWidget(self.label_12)
        self.label_5 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_5.setStyleSheet("background-color: #c3e7e3;")
        self.label_5.setText("")
        self.label_5.setObjectName("label_5")
        self.verticalLayout_5.addWidget(self.label_5)
        self.label_15 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_15.setStyleSheet("background-color: #b0dedc;")
        self.label_15.setText("")
        self.label_15.setObjectName("label_15")
        self.verticalLayout_5.addWidget(self.label_15)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_3.setStyleSheet("background-color: #c3e7e3;")
        self.label_3.setText("")
        self.label_3.setObjectName("label_3")
        self.verticalLayout_5.addWidget(self.label_3)
        self.label_19 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_19.setStyleSheet("background-color: #b0dedc;")
        self.label_19.setText("")
        self.label_19.setObjectName("label_19")
        self.verticalLayout_5.addWidget(self.label_19)
        self.label_10 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_10.setStyleSheet("background-color: #c3e7e3;")
        self.label_10.setText("")
        self.label_10.setObjectName("label_10")
        self.verticalLayout_5.addWidget(self.label_10)
        self.label_17 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_17.setStyleSheet("background-color: #b0dedc;")
        self.label_17.setText("")
        self.label_17.setObjectName("label_17")
        self.verticalLayout_5.addWidget(self.label_17)
        self.label_9 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_9.setStyleSheet("background-color: #c3e7e3;")
        self.label_9.setText("")
        self.label_9.setObjectName("label_9")
        self.verticalLayout_5.addWidget(self.label_9)
        self.label_7 = QtWidgets.QLabel(self.verticalLayoutWidget_3)
        self.label_7.setStyleSheet("background-color: #b0dedc;")
        self.label_7.setText("")
        self.label_7.setObjectName("label_7")
        self.verticalLayout_5.addWidget(self.label_7)
        self.verticalLayoutWidget_4 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(10+20, 75, 51, 391))
        self.verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_20 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_20.setFont(font)
        self.label_20.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-left-radius: 20px;")
        self.label_20.setObjectName("label_20")
        self.verticalLayout_6.addWidget(self.label_20)
        self.label_11 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_11.setFont(font)
        self.label_11.setStyleSheet("background-color: #b0dedc;")
        self.label_11.setObjectName("label_11")
        self.verticalLayout_6.addWidget(self.label_11)
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("background-color: #c3e7e3;")
        self.label_4.setObjectName("label_4")
        self.verticalLayout_6.addWidget(self.label_4)
        self.label_14 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_14.setFont(font)
        self.label_14.setStyleSheet("background-color: #b0dedc;")
        self.label_14.setObjectName("label_14")
        self.verticalLayout_6.addWidget(self.label_14)
        self.label_16 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_16.setFont(font)
        self.label_16.setStyleSheet("background-color: #c3e7e3;")
        self.label_16.setObjectName("label_16")
        self.verticalLayout_6.addWidget(self.label_16)
        self.label_13 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_13.setFont(font)
        self.label_13.setStyleSheet("background-color: #b0dedc;")
        self.label_13.setObjectName("label_13")
        self.verticalLayout_6.addWidget(self.label_13)
        self.label_18 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_18.setFont(font)
        self.label_18.setStyleSheet("background-color: #c3e7e3;")
        self.label_18.setObjectName("label_18")
        self.verticalLayout_6.addWidget(self.label_18)
        self.label_8 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("background-color: #b0dedc;")
        self.label_8.setObjectName("label_8")
        self.verticalLayout_6.addWidget(self.label_8)
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("background-color: #c3e7e3;")
        self.label_2.setObjectName("label_2")
        self.verticalLayout_6.addWidget(self.label_2)
        self.label_6 = QtWidgets.QLabel(self.verticalLayoutWidget_4)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("background-color: #b0dedc;\n"
"border: 1px solid #b0dedc;\n"
"border-bottom-left-radius: 20px;")
        self.label_6.setObjectName("label_6")
        self.verticalLayout_6.addWidget(self.label_6)
        self.verticalLayoutWidget_5 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(830+20, 75, 51, 391))
        self.verticalLayoutWidget_5.setObjectName("verticalLayoutWidget_5")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_22 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_22.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-top-right-radius: 20px;")
        self.label_22.setText("")
        self.label_22.setObjectName("label_22")
        self.verticalLayout_7.addWidget(self.label_22)
        self.label_23 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_23.setStyleSheet("background-color: #b0dedc;")
        self.label_23.setText("")
        self.label_23.setObjectName("label_23")
        self.verticalLayout_7.addWidget(self.label_23)
        self.label_24 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_24.setStyleSheet("background-color: #c3e7e3;")
        self.label_24.setText("")
        self.label_24.setObjectName("label_24")
        self.verticalLayout_7.addWidget(self.label_24)
        self.label_25 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_25.setStyleSheet("background-color: #b0dedc;")
        self.label_25.setText("")
        self.label_25.setObjectName("label_25")
        self.verticalLayout_7.addWidget(self.label_25)
        self.label_26 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_26.setStyleSheet("background-color: #c3e7e3;")
        self.label_26.setText("")
        self.label_26.setObjectName("label_26")
        self.verticalLayout_7.addWidget(self.label_26)
        self.label_27 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_27.setStyleSheet("background-color: #b0dedc;")
        self.label_27.setText("")
        self.label_27.setObjectName("label_27")
        self.verticalLayout_7.addWidget(self.label_27)
        self.label_28 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_28.setStyleSheet("background-color: #c3e7e3;")
        self.label_28.setText("")
        self.label_28.setObjectName("label_28")
        self.verticalLayout_7.addWidget(self.label_28)
        self.label_29 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_29.setStyleSheet("background-color: #b0dedc;")
        self.label_29.setText("")
        self.label_29.setObjectName("label_29")
        self.verticalLayout_7.addWidget(self.label_29)
        self.label_30 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_30.setStyleSheet("background-color: #c3e7e3;")
        self.label_30.setText("")
        self.label_30.setObjectName("label_30")
        self.verticalLayout_7.addWidget(self.label_30)
        self.label_31 = QtWidgets.QLabel(self.verticalLayoutWidget_5)
        self.label_31.setStyleSheet("background-color: #b0dedc;\n"
"border: 1px solid #b0dedc;\n"
"border-bottom-right-radius: 20px;")
        self.label_31.setText("")
        self.label_31.setObjectName("label_31")
        self.verticalLayout_7.addWidget(self.label_31)
        self.label_42 = QtWidgets.QLabel(self.centralwidget)
        self.label_42.setGeometry(QtCore.QRect(10+20, 475, 871, 41))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(14)
        font.setItalic(True)
        font.setStrikeOut(False)
        self.label_42.setFont(font)
        self.label_42.setStyleSheet("background-color: #c3e7e3;\n"
"border: 1px solid #c3e7e3;\n"
"border-radius: 20px;")
        self.label_42.setObjectName("label_42")
        self.label_43 = QtWidgets.QLabel(self.centralwidget)
        self.label_43.setGeometry(QtCore.QRect(890+20, 475, 51, 41))
        self.label_43.setStyleSheet("background-color: #c3e7e3;;\n"
"border: 1px solid #c3e7e3;;\n"
"border-bottom-right-radius: 20px;\n"
"border-top-right-radius: 20px;")
        self.label_43.setText("")
        self.label_43.setObjectName("label_43")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(410+20, 555, 117, 36))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.leftscroll_btn = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.leftscroll_btn.setMaximumSize(QtCore.QSize(16777215, 28))
        self.leftscroll_btn.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(".\\images/icons8-dvoynaya-strelka-vlevo-24.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.leftscroll_btn.setIcon(icon2)
        self.leftscroll_btn.setObjectName("leftscroll_btn")
        self.leftscroll_btn.setStyleSheet("QPushButton {\n"
                                          "border-style: solid;"
                                          "border-width: 1px;"
                                          "border-color: black;"
                                          "border-radius: 15px;"
                                          "border-bottom-left-radius: 14px;\n"
                                          "border-top-left-radius: 14px;"
                                          "}\n"
                                          "QPushButton:hover {\n"
                                          "     border-width: 0px;"
                                          "}\n"
                                          "QPushButton:pressed {\n"
                                          "     border-width: 1.5px;"
                                          "}\n")
        self.horizontalLayout.addWidget(self.leftscroll_btn)
        self.player_btn = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.player_btn.setMaximumSize(QtCore.QSize(16777215, 28))
        self.player_btn.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(".\\images/knopka_pley3.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.player_btn.setIcon(icon3)
        self.player_btn.setIconSize(QtCore.QSize(25, 22))
        self.player_btn.setObjectName("player_btn")
        self.player_btn.setStyleSheet("QPushButton {\n"
                                      "border-style: solid;"
                                      "border-width: 1px;"
                                      "border-color: black;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "     border-width: 0px;"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "     border-width: 1.5px;"
                                      "}\n")
        self.horizontalLayout.addWidget(self.player_btn)
        self.rigthscroll_btn = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.rigthscroll_btn.setMaximumSize(QtCore.QSize(16777215, 28))
        self.rigthscroll_btn.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(".\\images/icons8-dvoynaya-strelka-vpravo-24.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.rigthscroll_btn.setIcon(icon4)
        self.rigthscroll_btn.setObjectName("rigthscroll_btn")
        self.rigthscroll_btn.setStyleSheet("QPushButton {\n"
                                          "border-style: solid;"
                                          "border-width: 1px;"
                                          "border-color: black;"
                                          "border-radius: 15px;"
                                          "border-bottom-right-radius: 14px;\n"
                                          "border-top-right-radius: 14px;"
                                          "}\n"
                                          "QPushButton:hover {\n"
                                          "     border-width: 0px;"
                                          "}\n"
                                          "QPushButton:pressed {\n"
                                          "     border-width: 1.5px;"
                                          "}\n")
        self.horizontalLayout.addWidget(self.rigthscroll_btn)
        self.download_btn = QtWidgets.QPushButton(self.centralwidget)
        self.download_btn.setGeometry(QtCore.QRect(897+20, 480, 31, 31))
        self.download_btn.setStyleSheet("QPushButton {\n"
                                        "background-color: #c3e7e3;"
                                        "border-width: 1px;"
                                        "border-color: black;"
                                        "border-style: solid;"
                                        "border-bottom-right-radius: 14px;"
                                        "border-top-right-radius: 14px;"
                                        "}\n"
                                        "QPushButton:hover {\n"
                                        "      background: rgba(255, 255, 255, 0.3);\n"
                                        "}\n"
                                        "QPushButton:pressed {\n"
                                        "      border-width: 1.5px;"
                                        "}\n")
        self.download_btn.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(".\\images/knopka_skachivania2.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.download_btn.setIcon(icon5)
        self.download_btn.setObjectName("download_btn")
        self.play_btn_1 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_1.setGeometry(QtCore.QRect(290+20, 25, 31, 31))
        self.play_btn_1.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_1.setText("")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(".\\images/knopka_pley3.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.play_btn_1.setIcon(icon6)
        self.play_btn_1.setObjectName("play_btn_1")
        self.play_btn_4 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_4.setGeometry(QtCore.QRect(410+20, 25, 31, 31))
        self.play_btn_4.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_4.setText("")
        self.play_btn_4.setIcon(icon6)
        self.play_btn_4.setObjectName("play_btn_4")
        self.play_btn_3 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_3.setGeometry(QtCore.QRect(370+20, 25, 31, 31))
        self.play_btn_3.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_3.setText("")
        self.play_btn_3.setIcon(icon6)
        self.play_btn_3.setObjectName("play_btn_3")
        self.play_btn_5 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_5.setGeometry(QtCore.QRect(450+20, 25, 31, 31))
        self.play_btn_5.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_5.setText("")
        self.play_btn_5.setIcon(icon6)
        self.play_btn_5.setObjectName("play_btn_5")
        self.play_btn_8 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_8.setGeometry(QtCore.QRect(570+20, 25, 31, 31))
        self.play_btn_8.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_8.setText("")
        self.play_btn_8.setIcon(icon6)
        self.play_btn_8.setObjectName("play_btn_8")
        self.play_btn_7 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_7.setGeometry(QtCore.QRect(530+20, 25, 31, 31))
        self.play_btn_7.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_7.setText("")
        self.play_btn_7.setIcon(icon6)
        self.play_btn_7.setObjectName("play_btn_7")
        self.play_btn_9 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_9.setGeometry(QtCore.QRect(610+20, 25, 31, 31))
        self.play_btn_9.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_9.setText("")
        self.play_btn_9.setIcon(icon6)
        self.play_btn_9.setObjectName("play_btn_9")
        self.play_btn_2 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_2.setGeometry(QtCore.QRect(330+20, 25, 31, 31))
        self.play_btn_2.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_2.setText("")
        self.play_btn_2.setIcon(icon6)
        self.play_btn_2.setObjectName("play_btn_2")
        self.play_btn_10 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_10.setGeometry(QtCore.QRect(650+20, 25, 31, 31))
        self.play_btn_10.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_10.setText("")
        self.play_btn_10.setIcon(icon6)
        self.play_btn_10.setObjectName("play_btn_10")
        self.play_btn_6 = QtWidgets.QPushButton(self.centralwidget)
        self.play_btn_6.setGeometry(QtCore.QRect(490+20, 25, 31, 31))
        self.play_btn_6.setStyleSheet("QPushButton {\n"
                "background-color: #b0dedc;"
                                      "background-color: #b0dedc;"
                                      "border-style: inset;"
                                      "border-color: black;"
                                      "border-radius: 15px;"
                                      "border-width: 1px;"
                                      "}\n"
                                      "QPushButton:hover {\n"
                                      "    background: rgba(0, 0, 0, 0.1);\n"
                                      "}\n"
                                      "QPushButton:pressed {\n"
                                      "    border-style: outset;"
                                      "    border-width: 0px;"
                                      "    background: rgba(255, 255, 255, 0.1);\n"
                                      "}\n"
                                      )
        self.play_btn_6.setText("")
        self.play_btn_6.setIcon(icon6)
        self.play_btn_6.setObjectName("play_btn_6")
        self.check_btn_1 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_1.setGeometry(QtCore.QRect(290+20, 65, 31, 31))
        self.check_btn_1.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_1.setText("")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(".\\images/knopka2.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.check_btn_1.setIcon(icon7)
        self.check_btn_1.setObjectName("check_btn_1")
        self.check_btn_2 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_2.setGeometry(QtCore.QRect(330+20, 65, 31, 31))
        self.check_btn_2.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_2.setText("")
        self.check_btn_2.setIcon(icon7)
        self.check_btn_2.setObjectName("check_btn_2")
        self.check_btn_3 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_3.setGeometry(QtCore.QRect(370+20, 65, 31, 31))
        self.check_btn_3.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_3.setText("")
        self.check_btn_3.setIcon(icon7)
        self.check_btn_3.setObjectName("check_btn_3")
        self.check_btn_4 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_4.setGeometry(QtCore.QRect(410+20, 65, 31, 31))
        self.check_btn_4.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_4.setText("")
        self.check_btn_4.setIcon(icon7)
        self.check_btn_4.setObjectName("check_btn_4")
        self.check_btn_6 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_6.setGeometry(QtCore.QRect(490+20, 65, 31, 31))
        self.check_btn_6.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_6.setText("")
        self.check_btn_6.setIcon(icon7)
        self.check_btn_6.setObjectName("check_btn_6")
        self.check_btn_7 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_7.setGeometry(QtCore.QRect(530+20, 65, 31, 31))
        self.check_btn_7.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_7.setText("")
        self.check_btn_7.setIcon(icon7)
        self.check_btn_7.setObjectName("check_btn_7")
        self.check_btn_8 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_8.setGeometry(QtCore.QRect(570+20, 65, 31, 31))
        self.check_btn_8.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_8.setText("")
        self.check_btn_8.setIcon(icon7)
        self.check_btn_8.setObjectName("check_btn_8")
        self.check_btn_9 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_9.setGeometry(QtCore.QRect(610+20, 65, 31, 31))
        self.check_btn_9.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_9.setText("")
        self.check_btn_9.setIcon(icon7)
        self.check_btn_9.setObjectName("check_btn_9")
        self.check_btn_5 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_5.setGeometry(QtCore.QRect(450+20, 65, 31, 31))
        self.check_btn_5.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.1);\n"
"}\n"
"\n"
"")
        self.check_btn_5.setText("")
        self.check_btn_5.setIcon(icon7)
        self.check_btn_5.setObjectName("check_btn_5")
        self.check_btn_10 = QtWidgets.QPushButton(self.centralwidget)
        self.check_btn_10.setGeometry(QtCore.QRect(650+20, 65, 31, 31))
        self.check_btn_10.setStyleSheet("QPushButton {\n"
"background-color: #b0dedc;"
"border-style: none;"
"border-radius: 20px;"
"border-width: 1px;"
\
"}\n"
"\n"
"QPushButton:hover {\n"
"    border-radius: 12px;"
"    border-style: solid;\n"
"    border-color: black;"
"    border-width: 1px;"
                                       
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background: rgba(255, 255, 255, 0.3);\n"
"}\n"
"\n"
"")
        self.check_btn_10.setText("")
        self.check_btn_10.setIcon(icon7)
        self.check_btn_10.setObjectName("check_btn_10")
        self.qsl = QtWidgets.QSlider(self.centralwidget)
        self.qsl.setGeometry(QtCore.QRect(60+20, 525, 821, 22))
        self.qsl.setStyleSheet("background-color: #b0dedc;")
        self.qsl.setOrientation(QtCore.Qt.Horizontal)
        self.qsl.setObjectName("qsl")
        self.timer_start = QtWidgets.QLabel(self.centralwidget)
        self.timer_start.setGeometry(QtCore.QRect(10+20, 525, 41, 21))
        self.timer_start.setStyleSheet("")
        self.timer_start.setAlignment(QtCore.Qt.AlignCenter)
        self.timer_start.setObjectName("timer_start")
        self.timer_end = QtWidgets.QLabel(self.centralwidget)
        self.timer_end.setGeometry(QtCore.QRect(890+20, 525, 51, 21))
        self.timer_end.setStyleSheet("")
        self.timer_end.setScaledContents(False)
        self.timer_end.setAlignment(QtCore.Qt.AlignCenter)
        self.timer_end.setObjectName("timer_end")
        self.cur_tr_name = QtWidgets.QLabel(self.centralwidget)
        self.cur_tr_name.setGeometry(QtCore.QRect(10+20, 554, 391, 31))
        font = QtGui.QFont()
        font.setFamily("Papyrus")
        font.setPointSize(15)
        self.cur_tr_name.setFont(font)
        self.cur_tr_name.setText(self.lang_words['track_title'])
        self.cur_tr_name.setAlignment(QtCore.Qt.AlignCenter)
        self.cur_tr_name.setObjectName("cur_tr_name")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PMD"))
        self.menu.setTitle(_translate("MainWindow", self.lang_words['menu']))
        self.settings.setText(_translate("MainWindow", self.lang_words['settings']))
        self.about_us.setText(_translate("MainWindow", self.lang_words['about_us']))
        self.help.setText(_translate("MainWindow", self.lang_words['help']))
        self.cur_tr_name.setText(_translate("MainWindow", self.lang_words['track_title']))
        self.prev_page_btn.setText(_translate("MainWindow", "<<"))
        self.next_page_btn.setText(_translate("MainWindow", ">>"))

        self.label_42.setText(_translate("MainWindow", self.lang_words['slogan']))
        self.timer_start.setText(_translate("MainWindow", "00:00"))
        self.timer_end.setText(_translate("MainWindow", "00:00"))

    def setLang(self):
        self.config.read("settings.ini")
        self.lang_words = dict()

        if self.config.get('GLOBAL', 'LANG') == 'English':
            self.lang_words['menu'] = 'Menu'
            self.lang_words['settings'] = 'Settings'
            self.lang_words['about_us'] = 'About Us'
            self.lang_words['help'] = 'Help'
            self.lang_words['slogan'] = 'Pirate Music Downloader - We didn\'t try for the sake of donate'
            self.lang_words['track_title'] = 'Track title'
        else:
            self.lang_words['menu'] = 'Меню'
            self.lang_words['settings'] = 'Настройки'
            self.lang_words['about_us'] = 'О нас'
            self.lang_words['help'] = 'Справка'
            self.lang_words['slogan'] = 'Pirate Music Downloader - Мы старались не ради доната'
            self.lang_words['track_title'] = 'Название трека'
