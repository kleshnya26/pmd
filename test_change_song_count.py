import unittest
import main

from PyQt5 import QtWidgets
from settings import SettingsWindow

TEST_SONG_COUNT = 5


class TestChangeSongCount(unittest.TestCase):

    def setUp(self):
        self.app = QtWidgets.QApplication([])
        self.application = main.mywindow()
        self.application.setUI()
        self.application.show()
        self.old_song_count = self.application.SONG_COUNT
        self.application.settings_window = SettingsWindow(self.application.config)

    def test_change_song_count(self):
        self.application.settings_window.quant_tracks.setValue(TEST_SONG_COUNT)
        self.application.settings_window.setQuantTracks()
        self.application.changeQuantTracks()
        self.assertTrue(self.application.ui.play_btn_1.isVisible() and self.application.ui.play_btn_5.isVisible()
                        and not self.application.ui.play_btn_6.isVisible())
        self.application.settings_window.quant_tracks.setValue(self.old_song_count)
        self.application.settings_window.setQuantTracks()


if __name__ == '__main__':
    unittest.main()
