from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://muzfan.net/?do=search&subaction=search&story='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта muzfan.net (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='track-item', limit=position)

    try:
        text = items[-1].find('a', class_='track-title').get_text()
        name = text[:text.find(' -')]
        artist = text[text.find('- ') + 1:]
        url = items[-1].find('a', class_='track-dl')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
