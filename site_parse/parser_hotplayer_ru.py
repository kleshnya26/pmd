from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://hotplayer.ru/?s='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта hotplayer.ru (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='i', limit=position)

    try:

        name = items[-1].find('span', class_='tt').get_text()
        artist = items[-1].find('span', class_='title').a.get_text()
        url = items[-1].find('a', class_='dwnld fa fa-download')['href']
    except IndexError:
        return
    
    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
