from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://mp3fan.cc/music/'


def get_song_list(song_name, position, sq):
    """ Парсинг сайта mp3fan.cc (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='item', limit=position)

    try:
        name = items[-1].find('a', class_='title').get_text()
        artist = items[-1].find('a', class_='artist').get_text()
        url = 'https://' + items[-1].find('a', class_='button purple b-icon download')['onclick'].split('\'')[3][2:]
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
