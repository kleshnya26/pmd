from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://www.realrocks.ru/search/?q='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта realrocks.ru (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name + '&w=tracks'), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('li', class_='level-box', limit=position)

    try:
        artist = items[-1].find('a', class_='track__title').get_text()
        name = items[-1].find('div', class_='level-box sc__track-block').find('a', class_='track__title').get_text()
        url = items[-1].find('a', class_='track__toolbox-item', title='Cкачать mp3 бесплатно')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
