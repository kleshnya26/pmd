import requests
from requests.exceptions import RequestException


def get_song_html(url, song_name):
    """ Получение текста html страницы сайта с введенным запросом песни
    """
    try:
        req = requests.get(url + song_name)
    except RequestException:
        print('Ошибка запроса страницы ' + url + song_name)
        return ""

    return req.text
