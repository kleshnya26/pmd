from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://ru-drivemusic.net/?do=search&subaction=search&story='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта drivemusic.net (2 перехода)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='music-popular__item', limit=position)

    try:
        name = items[-1].find('a', class_='popular-play-composition').get_text()
        artist = items[-1].find('a', class_='popular-play-author').get_text()

        song_url = items[-1].find('a', class_='popular-download-link')['href']
        song_soup = BeautifulSoup(get_song_html('https://ru-drivemusic.net/' + song_url, ''), 'html.parser')
        url = 'https://ru-drivemusic.net/' + song_soup.find('a', class_='song-author-btn btn-download')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
