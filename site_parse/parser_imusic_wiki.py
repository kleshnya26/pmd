from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://imusic.xn--41a.wiki/search/'


def get_song_list(song_name, position, sq):
    """ Парсинг сайта imusic.я.wiki (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('li', class_='track', limit=position)

    try:
        name = items[-1].find('h2', class_='playlist-name').em.get_text()
        artist = items[-1].find('h2', class_='playlist-name').b.get_text()
        url = 'https://imusic.xn--41a.wiki' + items[-1].find('a', class_='playlist-btn-down no-ajaxy')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
