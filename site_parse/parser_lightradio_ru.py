from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://lightaudio.ru/mp3/'


def get_song_list(song_name, position, sq):
    """ Парсинг сайта lightaudio.ru (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='item', limit=position)

    try:
        name = items[-1].find('span', class_='title').get_text()
        artist = items[-1].find('span', class_='autor').a.get_text()
        url = 'https:' + items[-1].find('a', class_='down')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
