from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://ruq.hotmo.org/search?q='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта hotmo.org (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='track__info', limit=position)

    try:
        name = items[-1].find('div', class_='track__title').get_text(strip=True)
        artist = items[-1].find('div', class_='track__desc').get_text()
        url = items[-1].find('a', class_='track__download-btn')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
