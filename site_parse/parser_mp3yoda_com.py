from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://mp3yoda.com/search/'


def get_song_list(song_name, position, sq):
    """ Парсинг сайта mp3yoda.com (1 переход)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('li', itemprop='track', limit=position)

    try:
        name = items[-1].find('span', class_='playlist-name-title').em.get_text()
        artist = items[-1].find('span', class_='playlist-name-artist').b.get_text()
        url = items[-1].find('a', class_='songinfo-load')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
