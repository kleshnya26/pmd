from bs4 import BeautifulSoup
from .song_html import get_song_html

URL = 'https://backingtrackx.com/search.php?text='


def get_song_list(song_name, position, sq):
    """ Парсинг сайта backingtrackx.com (2 перехода)
    """

    soup = BeautifulSoup(get_song_html(URL, song_name), 'html.parser')

    name = None
    artist = None
    url = None

    items = soup.find_all('div', class_='pl-in', limit=position)

    try:
        name = items[-1].find('span', class_='songinlist').get_text()
        artist = items[-1].find('div', class_='isplink').get_text()

        song_url = items[-1].find('a', class_='downminus2')['href']
        song_soup = BeautifulSoup(get_song_html('https://backingtrackx.com/' + song_url, ''), 'html.parser')
        url = 'https://backingtrackx.com/' + song_soup.find('a', class_='downminus2')['href']
    except IndexError:
        return

    sq.put_nowait({'name': name, 'artist': artist, 'url': url})
