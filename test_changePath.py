import unittest
import main

import sys
import time
import re
import os
import shutil
import urllib
import urllib.error

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent

from main_window import Ui_MainWindow
from about_us import AboutUsWindow
from settings import SettingsWindow
from parser1 import Parser

import requests
import parser1
import threading
import configparser

class testChangePath(unittest.TestCase):

    def setUp(self):
        self.app = QtWidgets.QApplication([])
        self.application = main.mywindow()
        self.application.settings_window = SettingsWindow(self.application.config)

    def test_settings_change_path(self):
        self.cur_path = self.application.download_path
        self.application.download_path = os.getcwd()
        self.assertNotEqual(self.cur_path, self.application.download_path)

    @classmethod
    def tearDownClass(cls):
        pass

    if __name__ == '__main__':
        unittest.main()