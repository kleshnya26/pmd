import unittest
import os
import threading
import shutil
import main

from PyQt5 import QtWidgets

TEST_SONG_COUNT_1 = 10
TEST_MIN_SONG = 5
TEST_SONG_1 = 'Coffin dance'
TEST_DIR = "test_download"


class TestDownloadSong(unittest.TestCase):

    def setUp(self):
        self.app = QtWidgets.QApplication([])
        self.application = main.mywindow()
        self.application.setUI()
        self.application.SONG_COUNT = TEST_SONG_COUNT_1
        os.mkdir(TEST_DIR)
        self.application.download_path = TEST_DIR
        self.application.ui.search_line.setText(TEST_SONG_1)
        self.application.find()

    def test_download_song(self):
        self.application.checkClicked_1()
        self.application.checkClicked_2()
        self.application.checkClicked_3()
        self.application.checkClicked_4()
        self.application.checkClicked_5()
        self.application.checkClicked_6()
        self.application.checkClicked_7()
        self.application.checkClicked_8()
        self.application.checkClicked_9()
        self.application.checkClicked_10()
        self.application.download()

        for i in self.application.thread_list:
            i.join()

        self.assertGreater(len(os.listdir(path=self.application.download_path)), TEST_MIN_SONG)
        shutil.rmtree(path=self.application.download_path)


if __name__ == '__main__':
    unittest.main()
