import configparser
import os

from PyQt5 import QtCore, QtGui, QtWidgets


class SettingsWindow(QtWidgets.QWidget):
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.setLang()
        self.setupUi(self)


    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(658, 241)
        self.config = configparser.ConfigParser()
        self.quant_tracks = QtWidgets.QSpinBox(MainWindow)
        self.quant_tracks.setGeometry(QtCore.QRect(520, 52, 91, 31))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\images/accept_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.quant_tracks.setFont(font)
        self.quant_tracks.setObjectName("quant_tracks")
        self.qtr_comp = QtWidgets.QPushButton(MainWindow, clicked=self.setQuantTracks)
        self.qtr_comp.setGeometry(QtCore.QRect(615, 52, 27, 31))
        self.qtr_comp.setIcon(icon)
        self.qtr_comp.setObjectName("qtr_comp")
        self.change_lang = QtWidgets.QComboBox(MainWindow)
        self.change_lang.setGeometry(QtCore.QRect(520, 102, 91, 31))
        self.change_lang.setFont(font)
        self.change_lang.setObjectName("change_lang")
        self.change_lang.addItem("")
        self.change_lang.addItem("")
        self.chl_comp = QtWidgets.QPushButton(MainWindow, clicked=self.changeLang)
        self.chl_comp.setGeometry(QtCore.QRect(615, 102, 27, 31))
        self.chl_comp.setIcon(icon)
        self.chl_comp.setObjectName("chl_comp")
        self.path_btn = QtWidgets.QPushButton(MainWindow, clicked=self.changeDir)
        self.path_btn.setGeometry(QtCore.QRect(520, 152, 121, 31))
        self.path_btn.setText("...")
        self.path_btn.setObjectName("path_btn")
        self.label = QtWidgets.QLabel(MainWindow)
        self.label.setGeometry(QtCore.QRect(20, 52, 491, 31))
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(MainWindow)
        self.label_2.setGeometry(QtCore.QRect(20, 102, 491, 31))
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(MainWindow)
        self.label_3.setGeometry(QtCore.QRect(20, 152, 491, 31))
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", self.lang_words['settings']))
        self.change_lang.setItemText(0, _translate("MainWindow", "Русский"))
        self.change_lang.setItemText(1, _translate("MainWindow", "English"))
        self.label.setText(_translate("MainWindow", self.lang_words['found_song_num']))
        self.label_2.setText(_translate("MainWindow", self.lang_words['language']))
        self.label_3.setText(_translate("MainWindow", self.lang_words['up_tracks_folder']))

    def setQuantTracks(self):
        self.config.read("settings.ini")
        if not os.path.exists('settings.ini') or not self.config.has_section('GLOBAL'):
            self.config.add_section("GLOBAL")
        self.config.set("GLOBAL", "QUANTITY_TRACKS", str(self.quant_tracks.value()))
        self.config.write(open("settings.ini", "w"))

    def changeLang(self):
        self.config.read("settings.ini")
        if not os.path.exists('settings.ini') or not self.config.has_section('GLOBAL'):
            self.config.add_section("GLOBAL")
        if self.change_lang.currentIndex() == 0:
            current_lang = 'Russian'
        else:
            current_lang = 'English'
        self.config.set("GLOBAL", "LANG", current_lang)
        self.config.write(open("settings.ini", "w"))

    def changeDir(self):
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Выбрать папку", os.getcwd(), QtWidgets.QFileDialog.ShowDirsOnly)
        self.config.read("settings.ini")
        if not os.path.exists('settings.ini') or not self.config.has_section('GLOBAL'):
            self.config.add_section("GLOBAL")
        self.config.set("GLOBAL", "PATH", directory)
        self.config.write(open("settings.ini", "w"))

    def setLang(self):
        self.config.read("settings.ini")
        self.lang_words = dict()

        if self.config.get('GLOBAL', 'LANG') == 'English':
            self.lang_words['settings'] = 'Settings'
            self.lang_words['found_song_num'] = 'Number of songs found'
            self.lang_words['language'] = 'Language of the application'
            self.lang_words['up_tracks_folder'] = 'Folder for uploading tracks'
        else:
            self.lang_words['settings'] = 'Настройки'
            self.lang_words['found_song_num'] = 'Количество найденных песен'
            self.lang_words['language'] = 'Язык приложения'
            self.lang_words['up_tracks_folder'] = 'Папка для загрузки треков'


